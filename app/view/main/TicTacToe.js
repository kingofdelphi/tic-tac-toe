const getBtn = (row, col, onClick) => {
    return {
        xtype: 'button',
        text: '-',
        flex: 1,
        handler: function() {
            onClick(this, row, col);
        },
    };
};

const getButtons = (onClick) => {
    const objs = [];

    for (let i = 0; i < 3; i += 1) {
        const d = {
            xtype: 'button',
            text: `${i}`,
        };
        const box = Ext.create('Ext.Panel', {
            layout: {
                type: 'hbox',
            },
            items: [
                getBtn(i, 0, onClick),
                getBtn(i, 1, onClick),
                getBtn(i, 2, onClick),
            ]
        });
        objs.push(box);
    }
    return objs;
};

Ext.define('MyApp.view.main.TicTacToe', {
    extend: 'Ext.Panel',
    alias: 'board',
    controller: 'tictactoe',
    layout: { 
        type: 'vbox'
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'hbox'
            },
            padding: 20,
            items: [
                {
                    xtype: 'button',
                    text: 'Reset',
                    handler: 'reset',
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    handler: 'deleteBoard',
                },
                {
                    xtype: 'combobox',
                    label: 'First player',
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'name',
                    reference: 'firstPlayer',
                    editable: false, 
                    value: 'X',
                    store: [
                        { name: 'X' },
                        { name: 'O' },
                    ]
                },
                {
                    xtype: 'label',
                    html: 'Turn -',
                    style: {
                        'font-size': '18px',
                        'font-weight': 'bold',
                    },
                    reference: 'turn',
                },
            ],
        },
        {
            extend: 'Ext.form.Panel',
            reference: 'form',
            layout: {
                type: 'vbox',
            },
            listeners: {
                btnClick: 'btnClick',
                load: 'load',
                initialize: function () {
                    const handleClick = (btn, row, col) => {
                        this.fireEvent('btnClick', btn, row, col);
                    };
                    this.setItems(getButtons(handleClick));
                },
                painted: function () {
                    this.fireEvent('load');
                },
            },
        }]
});

