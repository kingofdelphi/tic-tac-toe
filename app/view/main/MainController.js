
Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    addBoard: function () {
        const view = this.getView();
        const comp = Ext.create('MyApp.view.main.TicTacToe');
        view.add(comp);
    },
});