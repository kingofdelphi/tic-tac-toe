Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.Panel',
    controller: 'main',
    layout: { 
        type: 'vbox'
    },
    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'hbox'
            },
            padding: 10,
            items: [
                {
                    xtype: 'button',
                    text: 'Add Board',
                    handler: 'addBoard',
                },
            ],
        },
    ]
});
