
//this data does not line up to our model fields - the phone field is called phoneNumber
const I = ' ';
const getNewBoard = (firstPlayer) => {
    return {
        board: [
            [I, I, I],
            [I, I, I],
            [I, I, I],
        ],
        cnt: 0,
        turn: firstPlayer,
    };
};

var data = getNewBoard('X');

const checkRowEqual = (row) => {
    const r = data.board[row];
    let p = data.board[row][0];
    if (p === I) p = 'hh';
    return r[1] === p && r[2] === p;
};

const checkColEqual = (col) => {
    const r = data.board;
    let p = data.board[0][col];
    if (p === I) p = 'hh';
    return r[1][col] === p && r[2][col] === p;
};

const checkDiagonals = () => {
    const r = data.board;
    let p = r[1][1];
    if (p === I) p = 'hh';
    return (r[0][0] === p && r[2][2] === p) ||
        (r[2][0] === p && r[0][2] === p);
};

const checkWin = () => {
    return checkRowEqual(0) ||
        checkRowEqual(1) ||
        checkRowEqual(2) ||
        checkColEqual(0) ||
        checkColEqual(1) ||
        checkColEqual(2) ||
        checkDiagonals();
};

Ext.define('MyApp.view.main.TicTacToeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.tictactoe',

    reset: function () {
        const form = this.lookupReference('form');
        const turn = this.lookupReference('turn');
        const firstPlayer = this.lookupReference('firstPlayer');
        const buttons = form.query('button');
        data = getNewBoard(firstPlayer.getValue());
        turn.setHtml(`Turn ${data.turn}`);
        buttons.forEach(btn => {
            btn.setText(I);
        });
    },
    deleteBoard: function () {
        this.getView().destroy();
    },
    load: function() {
        this.reset();
    },
    btnClick: function (button, row, col) {
        const txt = button._text;
        if (txt !== I) {
            return;
        }
        const { turn } = data;
        data.cnt += 1;
        data.board[row][col] = turn;
        button.setText(turn);
        if (checkWin()) {
            this.lookupReference('turn').setHtml(`${data.turn} wins`);
            return;
        }
        if (data.cnt === 9) {
            this.lookupReference('turn').setHtml(`Game finished to draw`);
            return;
        }
        data.turn = (turn === 'X' ? 'O' : 'X');
        this.lookupReference('turn').setHtml(`Turn ${data.turn}`);
    }
});